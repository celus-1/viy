#!/usr/bin/env python3
import click
import sys
from libviy import process_path


@click.command()
@click.argument('path', type=click.Path(), nargs=-1)
@click.option('iam_token', '--token', prompt='IAM Token')
@click.option('folder_id', '--folderid', prompt='Folder Id')
def viy(path, iam_token, folder_id):
    if not path:
        ctx = click.get_current_context()
        ctx.fail("At least one directory / file is expected")
    results = sum([process_path(item, iam_token, folder_id) for item in path],
                   start=[])
    for result in results:
        if not result.err:
            print(result.relative_path)
            print(result.text)
        else:
            print(result.relative_path)
            print(result.err_text, file=sys.stderr)

if __name__ == '__main__':
    viy(auto_envvar_prefix='VIY')
