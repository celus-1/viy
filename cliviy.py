#!/usr/bin/env python3
import click
import requests
import glob
import re
import os
import libviy
import sys
import urllib.parse


def viy_request_file(f, url, base_path=''):
    try:
        response = requests.post(url,
                                 files={"file" : open(f, "rb")})
    except IOError as e:
        return libviy.ViyResult(f,
                                base_dir=base_path,
                                err=True,
                                err_text=str(e))
    if response.status_code != 200:
        return libviy.ViyResult(f,
                                base_dir=base_path,
                                err=True,
                                err_text=response.text)
    return libviy.ViyResult(f,
                            base_dir=base_path,
                            text=response.text)

def viy_request_path(path, url):
    if os.path.isdir(path):
        regex = '|'.join([".*\." + ext for ext in libviy.ALLOWED_EXTENSIONS])
        results = []
        for item in glob.iglob(path + '/**/*', recursive=True):
            if re.match(regex, item):
                results.append(viy_request_file(item, url, path))
        return results
    else:
        return [viy_request_file(path, url)]

@click.command()
@click.argument('path', type=click.Path(), nargs=-1)
@click.option('protocol', '--protocol', default='http')
@click.option('host', '--host', prompt='Host')
@click.option('port', '--port', prompt='Port')
def cliviy(path, protocol, host, port):
    if not path:
        ctx = click.get_current_context()
        ctx.fail("At least one directory / file is expected")
    url = f"{protocol}://{host}:{port}"
    results = sum([viy_request_path(item, url) for item in path],
                   start=[])
    for result in results:
        if not result.err:
            print(result.relative_path)
            print(result.text)
        else:
            print(result.relative_path)
            print(result.err_text, file=sys.stderr)


if __name__ == '__main__':
    cliviy()

