import os
import re
import requests
import glob
import base64
import json


ALLOWED_EXTENSIONS = [
    'jpg',
    'jpeg',
    'png',
]

API_URL="https://vision.api.cloud.yandex.net/vision/v1/batchAnalyze"


class ViyResult:
    def __init__(self, path, text='', base_dir='', err=False, err_text=''):
        self.path = path
        self.base_dir = base_dir
        self.err = err
        self.err_text = err_text
        self.text = text
        if path.startswith(base_dir + '/'):
            self.relative_path = path[len(base_dir + '/'):]
        else:
            self.relative_path = path

def upload_data(token, folder_id, content):
    headers = {
        "Authorization" : f"Bearer {token}",
    }
    payload = {
        "analyzeSpecs" : [{
            "content" : content,
            "features" : [{
                "type" : "TEXT_DETECTION",
                "textDetectionConfig" : {
                    "languageCodes" : ["*"]
                }
            }]
        }],
        "folderId" : folder_id,
    }
    return requests.post(API_URL, headers=headers, json=payload)

def process_file(path, token, folder_id, base_dir=''):
    try:
        with open(path, "rb") as f:
            content = base64.b64encode(f.read()).decode('utf-8')
            try:
                response = upload_data(token, folder_id, content)
            except requests.exceptions.RequestException as e:
                return ViyResult(path=path,
                                 base_dir=base_dir,
                                 err=True,
                                 err_text=e)
            if response.status_code == 200:
                words = []
                body = response.json()
                results = body["results"][0]["results"][0]["textDetection"]
                pages = results["pages"]
                for page in pages:
                    for block in page["blocks"]:
                        for line in block["lines"]:
                            for word in line["words"]:
                                words.append(word["text"])
                return ViyResult(path=path,
                                 base_dir=base_dir,
                                 text=' '.join(words))
            try:
                err_message = response.json()["message"]
            except json.decoder.JSONDecodeError as e:
                code = response.status_code
                text = response.text
                err_message = f"server returned code {code}"
                if text: err_message += "with text: {text}"
            return ViyResult(path=path,
                             base_dir=base_dir,
                             err=True,
                             err_text=err_message)
    except IOError:
        err_text = f"Couldn't open / read file {path}"
        return ViyResult(path=path,
                         base_dir=base_dir,
                         err=True,
                         err_text=err_text)


def process_path(path, token, folder_id):
    if os.path.isdir(path):
        regex = '|'.join([".*\." + ext for ext in ALLOWED_EXTENSIONS])
        results = []
        for item in glob.iglob(path + '/**/*', recursive=True):
            if re.match(regex, item):
                results.append(process_file(item, token, folder_id, path))
        return results
    else:
        return [process_file(path, token, folder_id)]
