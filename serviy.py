#!/usr/bin/env python3
import base64
import json
import libviy
import requests
import os
from flask import Flask, request


app = Flask(__name__)
token=os.environ.get('SERVIY_OAUTH_TOKEN', None) or input('OAuth token: ')
folder_id=os.environ.get('SERVIY_FOLDER_ID', None) or input('Folder ID: ')

def get_iam_token(oauth_token):
    response = requests.post('https://iam.api.cloud.yandex.net/iam/v1/tokens',
                             json={"yandexPassportOauthToken": oauth_token})
    json_data = json.loads(response.text)
    if json_data is not None and 'iamToken' in json_data:
        return json_data['iamToken']
    return None


@app.route('/', methods=['POST']) 
def process_file():
    blob = request.files['file'].read()
    content = base64.b64encode(blob).decode('utf-8')
    iam_token = get_iam_token(token)
    if iam_token is None:
        return "Couldn't get IAM token", 500
    try:
        response = libviy.upload_data(iam_token, folder_id, content)
    except requests.exceptions.RequestException as e:
        return str(e), 500
    if response.status_code == 200:
        words = []
        body = response.json()
        results = body["results"][0]["results"][0]["textDetection"]
        pages = results["pages"]
        for page in pages:
            for block in page["blocks"]:
                for line in block["lines"]:
                    for word in line["words"]:
                         words.append(word["text"])
        return ' '.join(words), 200
    try:
        err_message = response.json()["message"]
    except json.decoder.JSONDecodeError as e:
        code = response.status_code
        text = response.text
        err_message = f"server returned code {code}"
        if text: err_message += "with text: {text}"
    return err_message, response.status_code

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8000)
