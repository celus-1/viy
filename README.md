# Мини-задание "Оцифровщик документов"

[Описание выполнения задания](docs/task.pdf)

![](docs/viy.jpg)

> Поднимите мне веки!

Задание выполнено в двух вариантах:

- standalone-приложение, которое позволяет подключиться к Yandex Vision с вашими IAM-токеном и идентификатором каталога
- клиент-серверное приложение, которое не требует токены только для развертки сервера; использование клиента требует только знание ip, на котором развернуто серверное приложение

## Библиотека libviy

Данная библиотека инкапсулирует весь функционал, необходимый для работы с Yandex Vision

## Приложение viy

Standalone-приложение, требует IAM Token и Folder ID для использования

Справка:

`python3 viy --help`

## Сервис serviy

Сервер, требует Yandex OAuth Token и Folder ID для использования. Поставляется вместе с сервисом serviy.service, для работы требуется указать OAuth Token и Folder ID

Запуск:

`python3 serviy.py`

## Клиент cliviy

Клиент для serviy, требует запущенного serbiy (удаленно или локально). Для использования необходимо знать только хост и порт запущенного serviy. Мой личный инстанс на момент публикации доступен публично, хост: `130.193.45.100`, порт: `8000`

Запуск:

`python3 cliviy.py`

Справка:

`python3 cliviy --help`
