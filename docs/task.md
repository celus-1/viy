---
lang: ru-RU
---

# Мини-задание "Оцифровщик документов"

## Задание

На вход подается путь до директории с изображениями

Программа поочередно обрабатывает все изображения и формирует итоговый файл с текстом, в следующем формате:

image1.png

<Тот текст, который удалось обнаружить на изображении.>

image2.jpeg

<Тот текст, который удалось обнаружить на изображении.>

Программа должна  находить картинки не только в указанной директории, но и во вложенных.

## Ход выполнения

Сначала написал простенькое cli-приложение viy и libviy. Они работают в паре: viy обеспечивает взаимодействие с пользователем, libviy отвечает за обработку файлов и соединение с Yandex Vision.

Затем разработал cliviy и serviy, которые дают возможность использовать клиент без Yandex Cloud, т.е. вся локига и токены для работы с Yandex Cloud переводятся на сервер, а клиенту необходимо знать только хост, на котором крутится сервер.

## Описание процесса работы приложения

Для всех взаимодействий с пользователем используется модуль click.

После получения необходимой информации при помощи модуля glob получаем все файлы в указанных директориях и их поддиректориях и формируем серию запросов либо к Yandex Vision, либо к serviy для клиентов viy и cliviy соответственно.

Получаем ответ от сервера, обрабатываем его, выводим результат. В случае ошибки, имя файла и текст ошибки выводятся в stderr.

## Пути улучшения

Можно добавить pdf; в базовом виде, необходимо просто добавить работу с MIME-типами и расширениями в serviy и viy соответственно, но поскольку в Yandex Vision ограничение и необходимо бить документы на куски по 8 страниц, полноценное выполнение этой фичи потребует еще час-два работы

## Время, затраченное на выполение

Около 6 часов

